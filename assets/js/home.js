var GLOBAL = {
  init: function () {
    this.slider();
    this.vitrina();
  },
  //función slider principal
  slider: function () {
    $('.slider-principal').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      speed: 300,
      arrows: false,
      autoplay: true,
      autoplayspeed: 1500,
    });
  },
  vitrina: function () {
    $('.product-slider ul').slick({
      dots: true,
      infinite: true,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4,
      autoplay: true,
      autoplayspeed: 1500,
      arrows: true,
      responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        }
      ]
    });
  }
}

$(document).ready(function () {
  GLOBAL.init();
});